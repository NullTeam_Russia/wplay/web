# Build stage
FROM node:latest 
WORKDIR /usr/app
COPY . /usr/app

RUN npm ci --silent && npm run build

# Runnig stage
FROM de1ay/nginx-http3:latest
COPY --from=0 /usr/app/dist /usr/app/dist

ARG RUN_MODE=production
ENV NODE_ENV $RUN_MODE

# Nginx configuration
ADD ./nginx/"${RUN_MODE}".conf /etc/nginx/nginx.conf

EXPOSE 80
EXPOSE 443
EXPOSE 443/udp

CMD ["/usr/sbin/nginx", "-g", "daemon off;"]
