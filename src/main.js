import Vue from 'vue';

import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import {
  faPen,
  faBars,
  faGift,
  faPlus,
  faMedal,
  faTrash,
  faUsers,
  faCheck,
  faTimes,
  faCrutch,
  faListUl,
  faSearch,
  faSpinner,
  faSadTear,
  faUserAlt,
  faChartBar,
  faSlidersH,
  faAngleDown,
  faArrowLeft,
  faUserSecret,
  faUserCircle,
  faUniversity,
  faSignOutAlt,
  faAddressCard,
  faExchangeAlt,
  faTachometerAlt,
} from '@fortawesome/free-solid-svg-icons';

import Toasted from 'vue-toasted';
import VueMasonry from 'vue-masonry-css';

import { directive as onClickOutside } from 'vue-on-click-outside';

import { apolloProvider } from '@/graphql';

import App from '@/App.vue';
import { router } from '@/router';
import store from '@/store';

// Main menu icons
library.add(faBars);
library.add(faCrutch);
library.add(faUserAlt);
library.add(faChartBar);
library.add(faSlidersH);
library.add(faTachometerAlt);

// OrgAdmin menu icons
library.add(faMedal);
library.add(faUsers);
library.add(faListUl);
library.add(faArrowLeft);
library.add(faUniversity);
library.add(faSignOutAlt);
library.add(faAddressCard);
library.add(faExchangeAlt);
library.add(faGift);

// OrgAdmin/Dashboard
library.add(faSadTear);

// Other icons
library.add(faPen);
library.add(faPlus);
library.add(faTrash);
library.add(faCheck);
library.add(faTimes);
library.add(faSearch);
library.add(faSpinner);
library.add(faAngleDown);
library.add(faUserSecret);
library.add(faUserCircle);

Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.directive('on-click-outside', onClickOutside);

Vue.use(Toasted, {
  duration: 2000,
  keepOnHover: true,
  theme: 'bubble',
  iconPack: 'fontawesome',
  className: 'wp-toast',
});
Vue.use(VueMasonry);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  apolloProvider,
  render: h => h(App),
}).$mount('#app');
