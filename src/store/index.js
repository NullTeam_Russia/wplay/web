import Vue from 'vue';
import Vuex from 'vuex';

import session from './session';
import admin from './admin';
import user from './user';
import organization from './organization';

Vue.use(Vuex);

const store = {
  state: {},
  modules: {
    session,
    admin,
    user,
    organization,
  },
};

export default new Vuex.Store(store);
