const mutations = {
  setOrganizationInfo(state, organization) {
    state.organization = organization;
  },
  setOrganizationMembers(state, members) {
    state.members = members;
  },
  setOrganizationRoles(state, roles) {
    state.roles = roles;
  },
  setOrganizationGroups(state, groups) {
    state.groups = groups;
  },
  setOrganizationReasons(state, reasons) {
    state.reasons = reasons;
  },
  setOrganizationCategories(state, categories) {
    state.categories = categories;
  },
};

export default mutations;
