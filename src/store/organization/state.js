const state = {
  organization: {
    name: '',
    categories_enabled: false,
    statistics_enabled: false,
    achievements_enabled: false,
    anonymization_enabled: false,
    teamplay_enabled: false,
    market_enabled: false,
    reasons_enabled: false,
  },
  members: [],
  roles: [],
  categories: [],
  reasons: [],
  groups: [],
};

export default state;
