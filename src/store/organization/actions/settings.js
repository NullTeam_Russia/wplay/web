import { apolloClient, parseGraphQLErrors } from '@/graphql';

import { OrganizationInfo } from '@/graphql/queries/organization';
import {
  EditOrganizationSettings,
  EditCategoriesEnabled,
  EditReasonsEnabled,
  EditTeamplayEnabled,
} from '@/graphql/mutations/organizations/info';

const actions = {
  async getOrganizationInfo(store) {
    const { commit, rootState } = store;
    return apolloClient.query({
      query: OrganizationInfo,
      variables: {
        id: rootState.session.selectedOrganization,
      },
    }).then((resp) => {
      const data = resp.data.Organization;
      commit('setOrganizationInfo', data);
      return Promise.resolve(data);
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
  async updateOrganizationSettings(store, organization) {
    const { rootState, dispatch } = store;
    return apolloClient.mutate({
      mutation: EditOrganizationSettings,
      variables: {
        organization_id: rootState.session.selectedOrganization,
        organization,
      },
    }).then((resp) => {
      dispatch('getOrganizationInfo');
      return Promise.resolve(resp.data.EditOrganization);
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
  async updateCategoriesEnabled(store, val) {
    const { rootState, dispatch } = store;
    return apolloClient.mutate({
      mutation: EditCategoriesEnabled,
      variables: {
        organization_id: rootState.session.selectedOrganization,
        categories_enabled: val,
      },
    }).then((resp) => {
      dispatch('getOrganizationInfo');
      return Promise.resolve(resp.data.EditOrganization);
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
  async updateReasonsEnabled(store, val) {
    const { rootState, dispatch } = store;
    return apolloClient.mutate({
      mutation: EditReasonsEnabled,
      variables: {
        organization_id: rootState.session.selectedOrganization,
        reasons_enabled: val,
      },
    }).then((resp) => {
      dispatch('getOrganizationInfo');
      return Promise.resolve(resp.data.EditOrganization);
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
  async updateTeamplayEnabled(store, val) {
    const { rootState, dispatch } = store;
    return apolloClient.mutate({
      mutation: EditTeamplayEnabled,
      variables: {
        organization_id: rootState.session.selectedOrganization,
        teamplay_enabled: val,
      },
    }).then((resp) => {
      dispatch('getOrganizationInfo');
      return Promise.resolve(resp.data.EditOrganization);
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
};

export default actions;
