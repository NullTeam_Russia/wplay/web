import { apolloClient, parseGraphQLErrors } from '@/graphql';

// Queries
import { OrganizationMembers } from '@/graphql/queries/organization';

// Mutations
import {
  CreateOrganizationInvite,
  DeleteOrganizationInvite,
} from '@/graphql/mutations/organizations/invite';
import { SetRole, UnsetRole } from '@/graphql/mutations/organizations/member';

const actions = {
  async getOrganizationMembers(store) {
    const { commit, rootState } = store;
    return apolloClient.query({
      query: OrganizationMembers,
      variables: {
        id: rootState.session.selectedOrganization,
      },
    }).then((resp) => {
      const data = resp.data.Organization.members;
      commit('setOrganizationMembers', data.data);
      return Promise.resolve({
        total_rows: data.total_rows,
        total_pages: data.total_pages,
      });
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
  async createOrganizationInvite(store, data) {
    const { rootState, dispatch } = store;
    return apolloClient.mutate({
      mutation: CreateOrganizationInvite,
      variables: {
        organization_id: rootState.session.selectedOrganization,
        ...data,
      },
    }).then((resp) => {
      dispatch('getOrganizationMembers');
      return Promise.resolve(resp.data.CreateOrganizationInvite);
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
  async deleteOrganizationInvite(store, userID) {
    const { rootState, dispatch } = store;
    return apolloClient.mutate({
      mutation: DeleteOrganizationInvite,
      variables: {
        organization_id: rootState.session.selectedOrganization,
        user_id: userID,
      },
    }).then((resp) => {
      dispatch('getOrganizationMembers');
      return Promise.resolve(resp.data.DeleteOrganizationInvite);
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
  async setRole(store, role) {
    const { dispatch } = store;
    return apolloClient.mutate({
      mutation: SetRole,
      variables: {
        user_id: role.user_id,
        role_id: role.id,
        display: role.display,
      },
    }).then((resp) => {
      dispatch('getOrganizationMembers');
      return Promise.resolve(resp.data.SetRole);
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
  async unsetRole(store, role) {
    const { dispatch } = store;
    return apolloClient.mutate({
      mutation: UnsetRole,
      variables: {
        user_id: role.user_id,
        role_id: role.id,
      },
    }).then((resp) => {
      dispatch('getOrganizationMembers');
      return Promise.resolve(resp.data.UnsetRole);
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
};

export default actions;
