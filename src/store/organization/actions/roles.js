import { apolloClient, parseGraphQLErrors } from '@/graphql';

import { OrganizationRoles } from '@/graphql/queries/organization';
import { CreateRole, EditRole, DeleteRole } from '@/graphql/mutations/organizations/role';

const actions = {
  async getOrganizationRoles(store) {
    const { commit, rootState } = store;
    return apolloClient.query({
      query: OrganizationRoles,
      variables: {
        id: rootState.session.selectedOrganization,
      },
    }).then((resp) => {
      const data = resp.data.Organization.roles;
      commit('setOrganizationRoles', data.data);
      return Promise.resolve({
        total_rows: data.total_rows,
        total_pages: data.total_pages,
      });
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
  async createRole(store, role) {
    const { rootState, dispatch } = store;
    return apolloClient.mutate({
      mutation: CreateRole,
      variables: {
        organization_id: rootState.session.selectedOrganization,
        role,
      },
    }).then((resp) => {
      dispatch('getOrganizationRoles');
      return Promise.resolve(resp.data.CreateRole);
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
  async updateRole(store, role) {
    const { dispatch } = store;
    return apolloClient.mutate({
      mutation: EditRole,
      variables: {
        role_id: role.id,
        role: {
          name: role.name,
        },
      },
    }).then((resp) => {
      dispatch('getOrganizationRoles');
      return Promise.resolve(resp.data.EditRole);
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
  async deleteRole(store, roleID) {
    const { dispatch } = store;
    return apolloClient.mutate({
      mutation: DeleteRole,
      variables: {
        role_id: roleID,
      },
    }).then((resp) => {
      dispatch('getOrganizationRoles');
      return Promise.resolve(resp.data.DeleteRole);
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
};

export default actions;
