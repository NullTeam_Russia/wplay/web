import { apolloClient, parseGraphQLErrors } from '@/graphql';

// Queries
import { OrganizationCategories } from '@/graphql/queries/organization';
// Mutations
import {
  CreateCategory,
  EditCategory,
  DeleteCategory,
} from '@/graphql/mutations/organizations/category';

const actions = {
  async getOrganizationCategories(store) {
    const { commit, rootState } = store;
    return apolloClient.query({
      query: OrganizationCategories,
      variables: {
        id: rootState.session.selectedOrganization,
      },
    }).then((resp) => {
      const data = resp.data.Organization.categories;
      commit('setOrganizationCategories', data.data);
      return Promise.resolve({
        total_rows: data.total_rows,
        total_pages: data.total_pages,
      });
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
  async createCategory(store, category) {
    const { rootState, dispatch } = store;
    return apolloClient.mutate({
      mutation: CreateCategory,
      variables: {
        organization_id: rootState.session.selectedOrganization,
        category,
      },
    }).then((resp) => {
      dispatch('getOrganizationCategories');
      return Promise.resolve(resp.data.CreateCategory);
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
  async updateCategory(store, category) {
    const { dispatch } = store;
    return apolloClient.mutate({
      mutation: EditCategory,
      variables: {
        category_id: category.id,
        category: {
          name: category.name,
          allow_negative: category.allow_negative,
        },
      },
    }).then((resp) => {
      dispatch('getOrganizationCategories');
      return Promise.resolve(resp.data.EditCategory);
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
  async deleteCategory(store, categoryID) {
    const { dispatch } = store;
    return apolloClient.mutate({
      mutation: DeleteCategory,
      variables: {
        category_id: categoryID,
      },
    }).then((resp) => {
      dispatch('getOrganizationCategories');
      return Promise.resolve(resp.data.DeleteCategory);
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
};

export default actions;
