import { apolloClient, parseGraphQLErrors } from '@/graphql';

import {
  CreateRolePermissions,
  EditRolePermission,
  DeleteRolePermission,
} from '@/graphql/mutations/organizations/permission';

const actions = {
  async createRolePermissions(store, rolePermissions) {
    const { dispatch } = store;
    return apolloClient.mutate({
      mutation: CreateRolePermissions,
      variables: {
        role_id: rolePermissions.role,
        role_permissions: rolePermissions.permissions,
      },
    }).then((resp) => {
      dispatch('getOrganizationRoles');
      return Promise.resolve(resp.data.CreateRolePermissions);
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
  async updateRolePermission(store, rolePermission) {
    const { dispatch } = store;
    return apolloClient.mutate({
      mutation: EditRolePermission,
      variables: {
        role_permission_id: rolePermission.id,
        role_permission: {
          type: rolePermission.type,
          self: rolePermission.self,
          resource: rolePermission.resource,
          create: rolePermission.create,
          read: rolePermission.read,
          update: rolePermission.update,
          delete: rolePermission.delete,
        },
      },
    }).then((resp) => {
      dispatch('getOrganizationRoles');
      return Promise.resolve(resp.data.EditRolePermission);
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
  async deleteRolePermission(store, rolePermissionID) {
    const { dispatch } = store;
    return apolloClient.mutate({
      mutation: DeleteRolePermission,
      variables: {
        role_permission_id: rolePermissionID,
      },
    }).then((resp) => {
      dispatch('getOrganizationRoles');
      return Promise.resolve(resp.data.DeleteRolePermission);
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
};

export default actions;
