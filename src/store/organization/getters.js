function convertArrayToObject(arr, key, prop) {
  return arr.reduce((result, item) => {
    const parsedItem = {};
    parsedItem[item[key]] = item[prop];
    return Object.assign(result, parsedItem);
  }, {});
}

const getters = {
  baseCategory: state => (state.organization.categories_enabled ? undefined : state.categories[0]),
  memberByID: state => convertArrayToObject(state.members, 'id', 'email'),
  categoryByID: state => convertArrayToObject(state.categories, 'id', 'name'),
  roleByID: state => convertArrayToObject(state.roles, 'id', 'name'),
  reasonByID: state => convertArrayToObject(state.reasons, 'id', 'text'),
  groupByID: state => convertArrayToObject(state.groups, 'id', 'name'),
};

export default getters;
