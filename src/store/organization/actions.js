import { apolloClient, parseGraphQLErrors } from '@/graphql';

import { Organization } from '@/graphql/queries/organization';

import Settings from './actions/settings';
import Members from './actions/members';
import Roles from './actions/roles';
import Permissions from './actions/permissions';
import Categories from './actions/categories';

const actions = {
  ...Settings,
  ...Members,
  ...Roles,
  ...Permissions,
  ...Categories,
  async getOrganization(store) {
    const { commit, rootState } = store;
    return apolloClient.query({
      query: Organization,
      variables: {
        id: rootState.session.selectedOrganization,
      },
    }).then((resp) => {
      const {
        members,
        roles,
        categories,
        reasons,
        groups,
      } = resp.data.Organization;
      commit('setOrganizationInfo', resp.data.Organization);
      commit('setOrganizationMembers', members.data);
      commit('setOrganizationRoles', roles.data);
      commit('setOrganizationCategories', categories.data);
      commit('setOrganizationReasons', reasons.data);
      commit('setOrganizationGroups', groups.data);
      return Promise.resolve();
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
};

export default actions;
