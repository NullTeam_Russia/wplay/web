/* eslint-disable no-param-reassign */
export function parseOrganizationPermissions(rolePermissions) {
  return rolePermissions.reduce((result, permission) => {
    if (!result[permission.type]) {
      result[permission.type] = {};
      result[permission.type][permission.resource] = {
        self: permission.self,
        create: permission.create,
        read: permission.read,
        update: permission.update,
        delete: permission.delete,
        permission_id: permission.id,
      };
    } else if (!result[permission.type][permission.resource]) {
      result[permission.type][permission.resource] = {
        self: permission.self,
        create: permission.create,
        read: permission.read,
        update: permission.update,
        delete: permission.delete,
        permission_id: permission.id,
      };
    } else {
      if (!result[permission.type][permission.resource].self) {
        result[permission.type][permission.resource].self = permission.self;
      }
      if (result[permission.type][permission.resource].create) {
        result[permission.type][permission.resource].create = permission.create;
      }
      if (result[permission.type][permission.resource].read) {
        result[permission.type][permission.resource].read = permission.read;
      }
      if (result[permission.type][permission.resource].update) {
        result[permission.type][permission.resource].update = permission.update;
      }
      if (result[permission.type][permission.resource].delete) {
        result[permission.type][permission.resource].delete = permission.delete;
      }
    }
    return result;
  }, {});
}

function setResourcePermissions(statePermissions, resourcePermissions) {
  if (statePermissions.create) {
    statePermissions.create = resourcePermissions.create;
  }
  if (statePermissions.read) {
    statePermissions.read = resourcePermissions.read;
  }
  if (statePermissions.update) {
    statePermissions.update = resourcePermissions.update;
  }
  if (statePermissions.delete) {
    statePermissions.delete = resourcePermissions.delete;
  }
}

function setTypePermissions(statePermissions, typePermissions) {
  Object.keys(typePermissions).forEach((permissionResource) => {
    const resourcePermissions = typePermissions[permissionResource];
    if (statePermissions[permissionResource]) {
      setResourcePermissions(statePermissions[permissionResource], resourcePermissions);
    } else {
      statePermissions[permissionResource] = resourcePermissions[permissionResource];
    }
  });
}

function setOrganizationPermissions(statePermissions, organizationPermissions) {
  Object.keys(organizationPermissions).forEach((permissionType) => {
    const typePermissions = organizationPermissions[permissionType];
    if (statePermissions[permissionType]) {
      setTypePermissions(statePermissions[permissionType], typePermissions);
    } else {
      statePermissions[permissionType] = organizationPermissions[permissionType];
    }
  });
}
/* eslint-enable */

const mutations = {
  setAuthorizedUser(state, user) {
    state.user.id = user.id;
    state.user.email = user.email;
  },
  setOrganizations(state, organizations) {
    state.organizations = organizations;
  },
  setInvitedOrganizations(state, invitedOrganizations) {
    state.invitedOrganizations = invitedOrganizations;
  },
  setPermissions(state, roles) {
    roles.forEach((role) => {
      const { organization } = role;
      const organizationPermissions = parseOrganizationPermissions(role.role_permissions.data);
      if (state.permissions[organization]) {
        setOrganizationPermissions(state.permissions[organization], organizationPermissions);
      } else {
        state.permissions[organization] = organizationPermissions;
      }
    });
  },
  selectOrganization(state, payload) {
    state.selectedOrganization = payload;
  },
};

export default mutations;
