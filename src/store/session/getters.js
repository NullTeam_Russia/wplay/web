/* eslint-disable dot-notation */
import { PermissionsTypes } from '@/shared';

const getters = {
  isAuthorized: state => !!state.user.id && !!state.user.id.length > 0,
  isUserInvitedInOrganization: state => state.invitedOrganizations.length !== 0,
  isUserInOrganization: state => state.organizations.length !== 0,
  isOrganizationSelected: state => !!state.selectedOrganization,
  // Permissions
  isUserAdmin: state => !!state.permissions['null']
    && !!state.permissions.null[PermissionsTypes.System]['null'].create
    && !!state.permissions.null[PermissionsTypes.System]['null'].read
    && !!state.permissions.null[PermissionsTypes.System]['null'].update
    && !!state.permissions.null[PermissionsTypes.System]['null'].delete,
};

export default getters;
