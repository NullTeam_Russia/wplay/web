import { apolloClient, parseGraphQLErrors } from '@/graphql';
import { User } from '@/graphql/queries/user';
import { Authorize, Register, Logout } from '@/graphql/mutations/session';

function getSelectedOrganizationFromLocalStorage() {
  return localStorage.getItem('selected_organization');
}

function updateSelectedOrganizationInLocalStorage(selectedOrganization) {
  localStorage.setItem('selected_organization', selectedOrganization);
}

function compareResourcePermissions(stateCRUD, requiredCRUD) {
  let result = true;
  if (requiredCRUD.create !== undefined) {
    result = result && (stateCRUD.create === requiredCRUD.create);
  }
  if (requiredCRUD.read !== undefined) {
    result = result && (stateCRUD.read === requiredCRUD.read);
  }
  if (requiredCRUD.update !== undefined) {
    result = result && (stateCRUD.update === requiredCRUD.update);
  }
  if (requiredCRUD.delete !== undefined) {
    result = result && (stateCRUD.delete === requiredCRUD.delete);
  }
  return result;
}

function compareTypePermissions(typePermissions, requiredPermission) {
  let result = false;
  if (requiredPermission.resource === undefined) {
    return Object.keys(typePermissions).some(resourceID => compareResourcePermissions(
      typePermissions[resourceID],
      requiredPermission.crud,
    ));
  }
  if (typePermissions[requiredPermission.resource]) {
    const stateCRUD = typePermissions[requiredPermission.resource];
    result = compareResourcePermissions(stateCRUD, requiredPermission.crud);
  }
  return result;
}

function compareOrganizationPermissions(organizationPermissions, requiredPermission) {
  let result = false;
  if (organizationPermissions[requiredPermission.type]) {
    const typePermissions = organizationPermissions[requiredPermission.type];
    result = compareTypePermissions(typePermissions, requiredPermission);
  }
  return result;
}

function compareStatePermissions(statePermissions, requiredPermission, isUserAdmin) {
  let result = false;
  if (statePermissions[requiredPermission.organization]) {
    const organizationPermissions = statePermissions[requiredPermission.organization];
    result = compareOrganizationPermissions(organizationPermissions, requiredPermission);
  }
  if (result) {
    if (requiredPermission.permissions && requiredPermission.permissions.length !== 0) {
      // eslint-disable-next-line
      result = result && IsAccessAllowed(statePermissions,
        requiredPermission.permissions, isUserAdmin);
    }
  }
  return result;
}

export function IsAccessAllowed(statePermissions, requiredPermissions, isUserAdmin) {
  if (isUserAdmin || requiredPermissions.length === 0) return true;
  return requiredPermissions.some(requiredPermission => compareStatePermissions(statePermissions,
    requiredPermission));
}

const actions = {
  getSelectedOrganization({ commit }) {
    const selectedOrganizaion = getSelectedOrganizationFromLocalStorage();
    commit('selectOrganization', selectedOrganizaion);
  },
  selectOrganization({ commit }, organizationID) {
    updateSelectedOrganizationInLocalStorage(organizationID);
    commit('selectOrganization', organizationID);
  },
  async register(store, user) {
    return apolloClient.mutate({
      mutation: Register,
      variables: {
        user,
      },
    }).then((resp) => {
      const data = resp.data.Register;
      return Promise.resolve(data);
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
  async authorize(store, credentials) {
    return apolloClient.mutate({
      mutation: Authorize,
      variables: {
        login: credentials.login,
        password: credentials.password,
      },
    }).then((resp) => {
      const data = resp.data.Authorize;
      return Promise.resolve(data);
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
  async logout() {
    return apolloClient.mutate({
      mutation: Logout,
    }).then((resp) => {
      const data = resp.data.Logout;
      return Promise.resolve(data);
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
  async getAuthorizedUser(store) {
    const { getters, commit, dispatch } = store;
    return apolloClient.query({
      query: User,
      variables: {
        user_id: null,
      },
    }).then((resp) => {
      const data = resp.data.User;
      commit('setAuthorizedUser', resp.data.User);
      commit('setPermissions', resp.data.User.roles.data);
      commit('setOrganizations', resp.data.User.organizations.data);
      commit('setInvitedOrganizations', resp.data.User.invited_organizations.data);
      if (resp.data.User.organizations.data.length === 1 && !getters.isUserAdmin) {
        dispatch('selectOrganization', resp.data.User.organizations.data[0].id);
      }
      return Promise.resolve(data);
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
};

export default actions;
