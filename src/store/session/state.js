const state = {
  selectedOrganization: '',
  user: {
    id: '',
    email: '',
  },
  permissions: {},
  organizations: [],
  invitedOrganizations: [],
};

export default state;
