const mutations = {
  setOrganizations(state, organizations) {
    state.organizations = organizations;
  },
};

export default mutations;
