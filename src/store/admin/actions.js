import { apolloClient, parseGraphQLErrors } from '@/graphql';

import { Organizations } from '@/graphql/queries/organization';
import { CreateOrganization, DeleteOrganization } from '@/graphql/mutations/organizations';

const actions = {
  async getOrganizations(store) {
    const { commit } = store;
    return apolloClient.query({
      query: Organizations,
    }).then((resp) => {
      commit('setOrganizations', resp.data.Organizations.data);
      return Promise.resolve();
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
  async createOrganization(store, organization) {
    const { dispatch } = store;
    return apolloClient.mutate({
      mutation: CreateOrganization,
      variables: {
        organization,
      },
    }).then(() => {
      dispatch('getOrganizations');
      return Promise.resolve();
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
  async deleteOrganization(store, organizationID) {
    const { dispatch } = store;
    return apolloClient.mutate({
      mutation: DeleteOrganization,
      variables: {
        organization_id: organizationID,
      },
    }).then(() => {
      dispatch('getOrganizations');
      return Promise.resolve();
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
};

export default actions;
