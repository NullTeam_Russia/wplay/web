// import { apolloClient, parseGraphQLErrors } from '@/graphql';

import IssuedGradeChanges from './actions/issued_grade_changes';

const actions = {
  ...IssuedGradeChanges,
};

export default actions;
