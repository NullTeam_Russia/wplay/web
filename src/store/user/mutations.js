const mutations = {
  setIssuedGradeChanges(state, issuedGradeChanges) {
    state.issued_grade_changes = issuedGradeChanges;
  },
};

export default mutations;
