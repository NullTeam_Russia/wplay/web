import { apolloClient, parseGraphQLErrors } from '@/graphql';

// Queries
import { IssuedGradeChanges } from '@/graphql/queries/user';
// Mutations
import { CreateGradeChange, DeleteGradeChange } from '@/graphql/mutations/user';

const actions = {
  async getIssuedGradeChanges(store, userID = null) {
    const { commit, rootState } = store;
    return apolloClient.query({
      query: IssuedGradeChanges,
      variables: {
        user_id: userID,
        organization_id: rootState.session.selectedOrganization,
      },
    }).then((resp) => {
      const data = resp.data.User.member.issued_grade_changes;
      commit('setIssuedGradeChanges', data.data);
      return Promise.resolve({
        total_rows: data.total_rows,
        total_pages: data.total_pages,
      });
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
  async createGradeChange(store, gradeChangeGroup) {
    const { rootState, dispatch } = store;
    return apolloClient.mutate({
      mutation: CreateGradeChange,
      variables: {
        organization_id: rootState.session.selectedOrganization,
        ...gradeChangeGroup,
      },
    }).then((resp) => {
      dispatch('getIssuedGradeChanges');
      return Promise.resolve(resp.data.CreateGradeChange);
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
  async deleteGradeChange(store, gradeChangeGroupID) {
    const { dispatch } = store;
    return apolloClient.mutate({
      mutation: DeleteGradeChange,
      variables: {
        grade_change_group: gradeChangeGroupID,
      },
    }).then((resp) => {
      dispatch('getIssuedGradeChanges');
      return Promise.resolve(resp.data.DeleteGradeChange);
    }).catch((resp) => {
      if (resp.graphQLErrors) {
        const errors = parseGraphQLErrors(resp);
        return Promise.reject(errors);
      }
      return Promise.reject(resp);
    });
  },
};

export default actions;
