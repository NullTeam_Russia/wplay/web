export const PermissionsTypes = {
  System: 1,
  Organization: 2,
  Members: 3,
  Roles: 4,
  Reasons: 5,
  Categories: 6,
  Groups: 7,
  Grades: 8,
};

export const PermissionsTypesNames = {
  1: 'Система',
  2: 'Организация',
  3: 'Участники',
  4: 'Роли',
  5: 'Причины',
  6: 'Категории',
  7: 'Команды',
  8: 'Оценки',
};

export const MinContentMargin = 60;
export const MenuWidth = 60;
export const MenuMargin = 60;
export const MasonryGlobalMargins = MinContentMargin * 2 + MenuWidth * 2 + MenuMargin * 2;
export const MasonryGutter = 30;
export const MasonryColumnWidth = 300;
export const MasonryColumnClass = 'masonry-column';

export const MasonryColumns = {};

for (let i = 1; i < 5; i += 1) {
  const columnsCount = i + 1;
  const columnsWidth = MasonryColumnWidth * columnsCount;
  const gapsWidth = MasonryGutter * i;
  const screenWidthChekpoint = MasonryGlobalMargins + gapsWidth + columnsWidth;
  MasonryColumns[screenWidthChekpoint] = i;
}
MasonryColumns.default = 5;

export default PermissionsTypes;
