import Vue from 'vue';
import VueApollo from 'vue-apollo';

import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';

Vue.use(VueApollo);

export function parseGraphQLErrors(errors) {
  return errors.graphQLErrors.map(err => ({
    code: err.extensions ? err.extensions.code : 401,
    message: err.message,
  }));
}

const apolloCache = new InMemoryCache();
const apolloLink = createHttpLink({
  uri: process.env.VUE_APP_API_ENDPOINT,
  credentials: 'include',
});

export const apolloClient = new ApolloClient({
  link: apolloLink,
  cache: apolloCache,
  defaultOptions: {
    query: {
      fetchPolicy: 'network-only',
    },
  },
});

export const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
  defaultOptions: {
    $query: {
      fetchPolicy: 'no-cache',
    },
  },
});

export default apolloProvider;
