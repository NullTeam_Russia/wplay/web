import gql from 'graphql-tag';

export const Organizations = gql`query Organizations {
  Organizations(page_array: { page: 0, per_page: 100000000 }) {
    data {
      id
      name
      categories_enabled
      reasons_enabled
      statistics_enabled
      achievements_enabled
      anonymization_enabled
      teamplay_enabled
      market_enabled
    }
  }
}`;

export const Organization = gql`query Organization($id: UUID!) {
  Organization(id: $id) {
    name
    categories_enabled
    reasons_enabled
    statistics_enabled
    achievements_enabled
    anonymization_enabled
    teamplay_enabled
    market_enabled
    members(page_array: { page: 0, per_page: 100000000 }) {
      total_pages
      total_rows
      data {
        id
        email
        img_path
        member(organization_id: $id) {
          is_confirmed
          roles(page_array: { page: 0, per_page: 100000000 }) {
            total_pages
            total_rows
            data {
              id
              name
              display
              organization
            }
          }
        }
      }
    }
    roles(page_array: { page: 0, per_page: 100000000 }) {
      total_pages
      total_rows
      data {
        id
        name
        display
        organization
        role_permissions(page_array: { page: 0, per_page: 100000000 }) {
          data {
            id
            type
            resource
            self
            create
            read
            update
            delete
          }
        }
      }
    }
    reasons(page_array: { page: 0, per_page: 100000000 }) {
      total_pages
      total_rows
      data {
        id
        text
        organization
        reason_grades {
          id
          reason
          category
          change
        }
      }
    }
    categories(page_array: { page: 0, per_page: 100000000 }) {
      total_pages
      total_rows
      data {
        id
        name
        base
        organization
        allow_negative
      }
    }
    groups(page_array: { page: 0, per_page: 100000000 }) {
      total_pages
      total_rows
      data {
        id
        name
        organization
        description
        img_path
      }
    }
  }
}`;

export const OrganizationInfo = gql`query OrganizationInfo($id: UUID!) {
  Organization(id: $id) {
    name
    categories_enabled
    reasons_enabled
    statistics_enabled
    achievements_enabled
    anonymization_enabled
    teamplay_enabled
    market_enabled
  }
}`;

export const OrganizationMembers = gql`query OrganizationMembers($id: UUID!) {
  Organization(id: $id) {
    members(page_array: { page: 0, per_page: 100000000 }) {
      total_pages
      total_rows
      data {
        id
        email
        img_path
        member(organization_id: $id) {
          is_confirmed
          roles(page_array: { page: 0, per_page: 100000000 }) {
            total_pages
            total_rows
            data {
              id
              name
              display
              organization
            }
          }
        }
      }
    }
  }
}`;

export const OrganizationRoles = gql`query OrganizationRoles($id: UUID!) {
  Organization(id: $id) {
    roles(page_array: { page: 0, per_page: 100000000 }) {
      total_pages
      total_rows
      data {
        id
        name
        display
        organization
        role_permissions(page_array: { page: 0, per_page: 100000000 }) {
          data {
            id
            type
            resource
            self
            create
            read
            update
            delete
          }
        }
      }
    }
  }
}`;

export const OrganizationCategories = gql`query OrganizationCategories($id: UUID!) {
  Organization(id: $id) {
    categories(page_array: { page: 0, per_page: 100000000 }) {
      total_pages
      total_rows
      data {
        id
        name
        base
        organization
        allow_negative
      }
    }
  }
}`;

export const OrganizationReasons = gql`query OrganizationReasons($id: UUID!) {
  Organization(id: $id) {
    reasons(page_array: { page: 0, per_page: 100000000 }) {
      total_pages
      total_rows
      data {
        id
        text
        organization
        reason_grades {
          id
          reason
          category
          change
        }
      }
    }
  }
}`;


export default {
  Organizations,
  Organization,
  OrganizationInfo,
  OrganizationMembers,
  OrganizationRoles,
  OrganizationCategories,
  OrganizationReasons,
};
