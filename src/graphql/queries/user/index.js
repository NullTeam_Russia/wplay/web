import gql from 'graphql-tag';

export const User = gql`query User($user_id: UUID) {
  User(user_id: $user_id) {
    id
    email
    organizations(page_array: {page: 0, per_page: 2147483647}) {
      data {
        id
        name
        members(page_array: {page: 0, per_page: 2147483647}) {
          total_pages
          total_rows
        }
      }
    }
    invited_organizations(page_array: {page: 0, per_page: 2147483647}) {
      data {
        id
        name
        reasons_enabled
        achievements_enabled
        anonymization_enabled
        categories_enabled
        market_enabled
        statistics_enabled
        teamplay_enabled
        members(page_array: {page: 0, per_page: 2147483647}) {
          total_pages
          total_rows
        }
      }
    }
    roles(page_array: {page: 0, per_page: 2147483647}) {
      data {
        id
        organization
        role_permissions(page_array: {page: 0, per_page: 2147483647}) {
          data {
            id
            type
            resource
            self
            create
            read
            update
            delete
          }
        }
      }
    }
  }
}`;

export const IssuedGradeChanges = gql`query IssuedGradeChanges($user_id: UUID, $organization_id: UUID!) {
  User(user_id: $user_id) {
    id
    member(organization_id: $organization_id) {
      issued_grade_changes(page_array: { page: 0, per_page: 10000000 }) {
        data {
          id
          text
          user
          grade_changes {
            category
            change
          }
        }
      }
    }
  }
}`;

export default {
  User,
  IssuedGradeChanges,
};
