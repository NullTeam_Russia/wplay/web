import gql from 'graphql-tag';

export const CreateGradeChange = gql`mutation CreateGradeChange($organization_id: UUID!, $user_id: UUID!, $text: String!, $grade_changes: [NewGradeChange!]!) {
  CreateGradeChange(organization_id: $organization_id, user_id: $user_id, text: $text, grade_changes: $grade_changes)
}`;

export const DeleteGradeChange = gql`mutation DeleteGradeChange($grade_change_group: UUID!) {
  DeleteGradeChange(grade_change_group: $grade_change_group)
}`;

export default {
  CreateGradeChange,
  DeleteGradeChange,
};
