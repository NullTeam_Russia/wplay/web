import gql from 'graphql-tag';

export const CreateOrganizationInvite = gql`mutation CreateOrganizationInvite(
  $organization_id: UUID!, $email: String!) {
  CreateOrganizationInvite(organization_id: $organization_id, email: $email)
}`;

export const AcceptOrganizationInvite = gql`mutation AcceptOrganizationInvite(
  $organization_id: UUID!) {
  AcceptOrganizationInvite(organization_id: $organization_id)
}`;

export const DeleteOrganizationInvite = gql`mutation DeleteOrganizationInvite(
  $organization_id: UUID!, $user_id: UUID!) {
  DeleteOrganizationInvite(organization_id: $organization_id, user_id: $user_id)
}`;

export default {
  CreateOrganizationInvite,
  AcceptOrganizationInvite,
  DeleteOrganizationInvite,
};
