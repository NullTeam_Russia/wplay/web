import gql from 'graphql-tag';

export const EditOrganizationSettings = gql`mutation EditOrganizationSettings(
  $organization_id: UUID!, $organization: EditedOrganization!) {
  EditOrganization(organization_id: $organization_id, organization: $organization)
}`;

export const EditCategoriesEnabled = gql`mutation EditCategoriesEnabled(
  $organization_id: UUID!, $categories_enabled: Boolean!) {
  EditOrganization(organization_id: $organization_id, organization: {
    categories_enabled: $categories_enabled
  })
}`;

export const EditReasonsEnabled = gql`mutation EditReasonsEnabled(
  $organization_id: UUID!, $reasons_enabled: Boolean!) {
  EditOrganization(organization_id: $organization_id, organization: {
    reasons_enabled: $reasons_enabled
  })
}`;

export const EditTeamplayEnabled = gql`mutation EditTeamplayEnabled(
  $organization_id: UUID!, $teamplay_enabled: Boolean!) {
  EditOrganization(organization_id: $organization_id, organization: {
    teamplay_enabled: $teamplay_enabled
  })
}`;

export default {
  EditOrganizationSettings,
  EditCategoriesEnabled,
  EditReasonsEnabled,
  EditTeamplayEnabled,
};
