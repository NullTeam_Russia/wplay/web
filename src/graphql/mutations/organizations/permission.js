import gql from 'graphql-tag';

export const CreateRolePermissions = gql`mutation CreateRolePermissions(
  $role_id: UUID!, $role_permissions: [NewRolePermission!]!) {
  CreateRolePermissions(role_id: $role_id, role_permissions: $role_permissions)
}`;

export const EditRolePermission = gql`mutation EditRolePermission(
  $role_permission_id: UUID!, $role_permission: EditedRolePermission!) {
  EditRolePermission(
    role_permission_id: $role_permission_id,
    role_permission: $role_permission,
  )
}`;

export const DeleteRolePermission = gql`mutation DeleteRolePermission(
  $role_permission_id: UUID!) {
  DeleteRolePermission(role_permission_id: $role_permission_id)
}`;

export default {
  CreateRolePermissions,
  EditRolePermission,
  DeleteRolePermission,
};
