import gql from 'graphql-tag';

export const CreateRole = gql`mutation CreateRole(
  $organization_id: UUID!, $role: NewRole!) {
  CreateRole(organization_id: $organization_id, role: $role) {
    id
  }
}`;

export const EditRole = gql`mutation EditRole(
  $role_id: UUID!, $role: EditedRole!) {
  EditRole(role_id: $role_id, role: $role)
}`;

export const DeleteRole = gql`mutation DeleteRole(
  $role_id: UUID!) {
  DeleteRole(role_id: $role_id)
}`;

export default {
  CreateRole,
  EditRole,
  DeleteRole,
};
