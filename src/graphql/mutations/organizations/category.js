import gql from 'graphql-tag';

export const CreateCategory = gql`mutation CreateCategory(
  $organization_id: UUID!, $category: NewCategory!) {
  CreateCategory(organization_id: $organization_id, category: $category) {
    id
  }
}`;

export const EditCategory = gql`mutation EditCategory(
  $category_id: UUID!, $category: EditedCategory!) {
  EditCategory(category_id: $category_id, category: $category)
}`;

export const DeleteCategory = gql`mutation DeleteCategory(
  $category_id: UUID!) {
  DeleteCategory(category_id: $category_id)
}`;

export default {
  CreateCategory,
  EditCategory,
  DeleteCategory,
};
