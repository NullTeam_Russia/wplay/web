import gql from 'graphql-tag';

export const SetRole = gql`mutation SetRole(
  $user_id: UUID!, $role_id: UUID!, $display: Boolean!) {
  SetRole(user_id: $user_id, role_id: $role_id, display: $display)
}`;

export const UnsetRole = gql`mutation UnsetRole(
  $user_id: UUID!, $role_id: UUID!) {
  UnsetRole(user_id: $user_id, role_id: $role_id)
}`;

export default {
  SetRole,
  UnsetRole,
};
