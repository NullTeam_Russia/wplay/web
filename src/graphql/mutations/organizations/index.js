import gql from 'graphql-tag';

export const CreateOrganization = gql`mutation CreateOrganization(
  $organization: NewOrganization!) {
  CreateOrganization(organization: $organization)
}`;

export const DeleteOrganization = gql`mutation DeleteOrganization(
  $organization_id: UUID!) {
  DeleteOrganization(organization_id: $organization_id)
}`;

export default {
  CreateOrganization,
  DeleteOrganization,
};
