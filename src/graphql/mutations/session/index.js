import gql from 'graphql-tag';

export const Authorize = gql`mutation Authorize($login: String!, $password: String!) {
  Authorize(login: $login, password: $password)
}`;

export const Register = gql`mutation Register($user: NewUser!) {
  Register(user: $user) 
}`;

export const Logout = gql`mutation Logout {
  Logout
}`;

export default {
  Authorize,
  Register,
  Logout,
};
