import Vue from 'vue';
import VueRouter from 'vue-router';

import store from '@/store';

import Layout from '@/layouts/Base.vue';

import Login from '@/views/auth/Login.vue';
import Logout from '@/views/auth/Logout.vue';
import Registration from '@/views/auth/Registration.vue';

import Profile from '@/views/portal/Profile.vue';
import Dashboard from '@/views/portal/Dashboard.vue';
import IssuedGardeChanges from '@/views/portal/IssuedGradeChanges.vue';
import Statistics from '@/views/portal/Statistics.vue';

// Admin
import AdminOrganizations from '@/views/portal/admin/Organizations.vue';

// OrgAdmin
import OrganizationUsers from '@/views/portal/orgadmin/Users.vue';
import OrganizationRoles from '@/views/portal/orgadmin/Roles.vue';
import OrganizationDashboard from '@/views/portal/orgadmin/Dashboard.vue';
import OrganizationCategories from '@/views/portal/orgadmin/Categories.vue';
import OrganizationAchievemtns from '@/views/portal/orgadmin/Achievements.vue';
import OrganizationMarket from '@/views/portal/orgadmin/Market.vue';
import OrganizationReasons from '@/views/portal/orgadmin/Reasons.vue';
import OrganizationStatistics from '@/views/portal/orgadmin/Statistics.vue';
import OrganizationGroups from '@/views/portal/orgadmin/Groups.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'index',
    redirect: { name: 'login' },
  },
  {
    path: '/registration',
    name: 'registration',
    component: Registration,
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
  },
  {
    path: '/logout',
    name: 'logout',
    component: Logout,
  },
  {
    path: '/portal',
    name: 'portal',
    component: Layout,
    redirect: { name: 'dashboard' },
    children: [
      {
        path: 'admin',
        name: 'admin',
        component: null,
        redirect: { name: 'admin/organizations' },
      },
      {
        path: 'admin/organizations',
        name: 'admin/organizations',
        component: AdminOrganizations,
      },
      {
        path: 'orgadmin',
        name: 'orgadmin',
        component: null,
        redirect: { name: 'orgadmin/dashboard' },
      },
      {
        path: 'orgadmin/dashboard',
        name: 'orgadmin/dashboard',
        component: OrganizationDashboard,
      },
      {
        path: 'orgadmin/users',
        name: 'orgadmin/users',
        component: OrganizationUsers,
      },
      {
        path: 'orgadmin/roles',
        name: 'orgadmin/roles',
        component: OrganizationRoles,
      },
      {
        path: 'orgadmin/categories',
        name: 'orgadmin/categories',
        component: OrganizationCategories,
      },
      {
        path: 'orgadmin/groups',
        name: 'orgadmin/groups',
        component: OrganizationGroups,
      },
      {
        path: 'orgadmin/reasons',
        name: 'orgadmin/reasons',
        component: OrganizationReasons,
      },
      {
        path: 'orgadmin/market',
        name: 'orgadmin/market',
        component: OrganizationMarket,
      },
      {
        path: 'orgadmin/achievements',
        name: 'orgadmin/achievements',
        component: OrganizationAchievemtns,
      },
      {
        path: 'orgadmin/statistics',
        name: 'orgadmin/statistics',
        component: OrganizationStatistics,
      },
      {
        path: 'dashboard',
        name: 'dashboard',
        component: Dashboard,
      },
      {
        path: 'profile',
        name: 'profile',
        component: Profile,
      },
      {
        path: 'issued_grade_changes',
        name: 'issued_grade_changes',
        component: IssuedGardeChanges,
      },
      {
        path: 'statistics',
        name: 'statistics',
        component: Statistics,
      },
    ],
  },
];

export const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach(async (to, from, next) => {
  if (to.name !== 'login' && to.name !== 'logout' && to.name !== 'registration') {
    try {
      await store.dispatch('session/getAuthorizedUser');
      if (!store.getters['session/isAuthorized']) {
        window.location.replace('/logout');
      }
      store.dispatch('session/getSelectedOrganization');
      if (store.getters['session/isOrganizationSelected']) {
        store.dispatch('organization/getOrganizationInfo');
      }
    } catch (e) {
      if (e[0] && e[0].code === 401) {
        window.location.replace('/logout');
      }
    }
  }
  next();
});

export default {
  router,
};
