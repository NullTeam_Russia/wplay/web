module.exports = {
  client: {
    service: {
      name: 'wplay',
      url: 'http://localhost:8081/query',
    },
    includes: [
      'src/**/*.vue',
      'src/**/*.js',
    ],
  },
}
