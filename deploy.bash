#!/bin/bash
while getopts i:u:r:p:s: option
do
case "${option}"
in
u) USER=${OPTARG};;
p) PASSWORD=${OPTARG};;
r) REGISTRY=${OPTARG};;
i) IMAGE=${OPTARG};;
esac
done
docker login -u $USER -p $PASSWORD $REGISTRY
if docker ps | grep -q wplay-web
then
docker stop wplay-web
docker rm wplay-web
docker pull $IMAGE:latest
fi
docker run -d -p 443:443/udp -p 443:443/tcp -p 80:80/tcp --restart=always --name wplay-web -v /usr/app/ssl:/usr/app/ssl -v /var/www/_letsencrypt:/var/www/_letsencrypt $IMAGE:latest
docker image prune -f